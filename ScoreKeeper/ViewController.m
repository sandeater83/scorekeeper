//
//  ViewController.m
//  ScoreKeeper
//
//  Created by Stuart Adams on 1/14/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //storing commonly used device properties
    int screenWidth = self.view.frame.size.width;
    int screenHeight = self.view.frame.size.height;
    
    self.view.backgroundColor = [UIColor blackColor];
    
    //Initializing scores of player one and two
    playerOneLbl = [[UILabel alloc]initWithFrame:CGRectMake((screenWidth/4) - 25, (screenHeight/2)- 75, 100, 100)];
    playerOneLbl.text = @"0";
    playerOneLbl.textColor = [UIColor whiteColor];
    playerOneLbl.font = [UIFont systemFontOfSize:50];
    
    playerTwoLbl = [[UILabel alloc]initWithFrame:CGRectMake((screenWidth * .75) - 25, (screenHeight/2)- 75, 100, 100)];
    playerTwoLbl.text = @"0";
    playerTwoLbl.textColor = [UIColor whiteColor];
    playerTwoLbl.font = [UIFont systemFontOfSize:50];
    
    //Initializing the title for the app
    UILabel* title = [[UILabel alloc] initWithFrame:CGRectMake(screenWidth/2 - 110, 20, 300, 50)];
    title.textColor = [UIColor cyanColor];
    title.text = @"SCORE KEEPER";
    title.font = [UIFont systemFontOfSize:30];
    
    //adding labels to view
    [self.view addSubview:title];
    [self.view addSubview:playerOneLbl];
    [self.view addSubview:playerTwoLbl];
    
    //Creating stepper controls for both players
    UIStepper* playerOneStepper = [[UIStepper alloc]initWithFrame:CGRectMake(playerOneLbl.frame.origin.x - 25, playerOneLbl.frame.origin.y + 100, 100, 100)];
    
    playerOneStepper.tintColor = [UIColor whiteColor];
    
    //Adding target function to stepper
    [playerOneStepper addTarget:self action:@selector(playerOneStepperTouched:) forControlEvents: UIControlEventTouchUpInside];
    
    UIStepper* playerTwoStepper = [[UIStepper alloc]initWithFrame:CGRectMake(playerTwoLbl.frame.origin.x - 25, playerTwoLbl.frame.origin.y + 100, 100, 100)];
    
    playerTwoStepper.tintColor = [UIColor whiteColor];
    //Adding target function to stepper
    [playerTwoStepper addTarget:self action:@selector(playerTwoStepperTouched:) forControlEvents: UIControlEventTouchUpInside];
    
    //Adding steppers to the view
    [self.view addSubview:playerOneStepper];
    [self.view addSubview:playerTwoStepper];
    
    //Creating text field for player names
    UITextField* playerOneName = [[UITextField alloc]initWithFrame:CGRectMake(screenWidth/4 - 60, screenHeight/4 - 20, 110, 50)];
    //styling the text field
    playerOneName.textColor = [UIColor whiteColor];
    playerOneName.text = @"Player 1";
    playerOneName.font = [UIFont systemFontOfSize:25];
    playerOneName.borderStyle = UITextBorderStyleRoundedRect;
    playerOneName.backgroundColor = [UIColor blueColor];
    
    //Creating text field for player names
    UITextField* playerTwoName = [[UITextField alloc]initWithFrame:CGRectMake(screenWidth * .75 - 60, screenHeight/4 - 20, 110, 50)];
    //styling the text field
    playerTwoName.textColor = [UIColor whiteColor];
    playerTwoName.text = @"Player 2";
    playerTwoName.font = [UIFont systemFontOfSize:25];
    playerTwoName.borderStyle = UITextBorderStyleRoundedRect;
    playerTwoName.backgroundColor = [UIColor blueColor];
    
    //adding text fields to the view
    [self.view addSubview:playerOneName];
    [self.view addSubview:playerTwoName];
    
    //creating score reset button
    UIButton* resetScore = [[UIButton alloc]initWithFrame:CGRectMake(screenWidth/2 - 75, screenHeight * .75, 150, 50)];
    
    //adding target function to button
    [resetScore addTarget:self action:@selector(resetPlayerScores) forControlEvents:UIControlEventTouchUpInside];
    
    //Styling button
    [resetScore setTitle:@"Reset Score" forState:UIControlStateNormal];
    resetScore.backgroundColor = [UIColor blueColor];
    
    //adding button to view
    [self.view addSubview:resetScore];
}

//Functions for player one and player two stepper controls
-(void)playerOneStepperTouched:(UIStepper*)playerOneStepper
{
    playerOneLbl.text = [NSString stringWithFormat:@"%.f", playerOneStepper.value];
}

-(void)playerTwoStepperTouched:(UIStepper*)playerTwoStepper
{
    playerTwoLbl.text = [NSString stringWithFormat:@"%.f", playerTwoStepper.value];
}

//Function to reset the scores to 0
-(void)resetPlayerScores
{
    playerOneLbl.text = @"0";
    playerTwoLbl.text = @"0";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
